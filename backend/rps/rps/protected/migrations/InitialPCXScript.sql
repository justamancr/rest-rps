SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

DROP DATABASE IF EXISTS pcx;
CREATE DATABASE `pcx` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `pcx`;

CREATE TABLE IF NOT EXISTS `address` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `id_list_country` char(4) collate utf8_unicode_ci default NULL,
  `id_list_state` int(11) unsigned default NULL,
  `city` varchar(64) collate utf8_unicode_ci default NULL,
  `address01` varchar(256) collate utf8_unicode_ci default NULL,
  `address02` varchar(256) collate utf8_unicode_ci default NULL,
  `zip` varchar(64) collate utf8_unicode_ci default NULL,
  PRIMARY KEY  (`id`),
  KEY `id_list_state` (`id_list_state`),
  KEY `id_list_country` (`id_list_country`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `admin_method` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(64) NOT NULL,
  `rank` int(11) NOT NULL,
  `status` varchar(16) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

INSERT INTO `admin_method` (`id`, `name`, `rank`, `status`) VALUES
(null, 'Admin Method test 1', 0, ''),
(null, 'Admin Method test 2', 0, '');


CREATE TABLE IF NOT EXISTS `company` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `id_address` int(11) unsigned NOT NULL,
  `alias` varchar(32) character set utf8 collate utf8_unicode_ci NOT NULL,
  `name` varchar(128) character set utf8 collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `address_id` (`id_address`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `list_country` (
  `id` char(4) collate utf8_unicode_ci NOT NULL,
  `name` varchar(64) collate utf8_unicode_ci NOT NULL,
  `status` varchar(16) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;

INSERT INTO `list_country` (`id`, `name`, `status`) VALUES
('ABW', 'ARUBA', 'OK'),
('AFG', 'AFGHANISTAN', 'OK'),
('AGO', 'ANGOLA', 'OK'),
('AIA', 'ANGUILLA', 'OK'),
('ALB', 'ALBANIA', 'OK'),
('AND', 'ANDORRA', 'OK'),
('ANT', 'NETHERLANDS ANTILLES', 'OK'),
('ARE', 'UNITED ARAB EMIRATES', 'OK'),
('ARG', 'ARGENTINA', 'OK'),
('ARM', 'ARMENIA', 'OK'),
('ASM', 'AMERICAN SAMOA', 'OK'),
('ATA', 'ANTARCTICA', 'OK'),
('ATF', 'FRENCH SOUTHERN TERRITORIES', 'OK'),
('ATG', 'ANTIGUA AND BARBUDA', 'OK'),
('AUS', 'AUSTRALIA', 'OK'),
('AUT', 'AUSTRIA', 'OK'),
('AZE', 'AZERBAIJAN', 'OK'),
('BDI', 'BURUNDI', 'OK'),
('BEL', 'BELGIUM', 'OK'),
('BEN', 'BENIN', 'OK'),
('BFA', 'BURKINA FASO', 'OK'),
('BGD', 'BANGLADESH', 'OK'),
('BGR', 'BULGARIA', 'OK'),
('BHR', 'BAHRAIN', 'OK'),
('BHS', 'BAHAMAS', 'OK'),
('BIH', 'BOSNIA AND HERZEGOWINA', 'OK'),
('BLR', 'BELARUS', 'OK'),
('BLZ', 'BELIZE', 'OK'),
('BMU', 'BERMUDA', 'OK'),
('BOL', 'BOLIVIA', 'OK'),
('BRA', 'BRAZIL', 'OK'),
('BRB', 'BARBADOS', 'OK'),
('BRN', 'BRUNEI DARUSSALAM', 'OK'),
('BTN', 'BHUTAN', 'OK'),
('BVT', 'BOUVET ISLAND', 'OK'),
('BWA', 'BOTSWANA', 'OK'),
('CAF', 'CENTRAL AFRICAN REPUBLIC', 'OK'),
('CAN', 'CANADA', 'OK'),
('CCK', 'COCOS (KEELING) ISLANDS', 'OK'),
('CHE', 'SWITZERLAND', 'OK'),
('CHL', 'CHILE', 'OK'),
('CHN', 'CHINA', 'OK'),
('CIV', 'COTE DIVOIRE', 'OK'),
('CMR', 'CAMEROON', 'OK'),
('COD', 'CONGO, Democratic Republic of (w', 'OK'),
('COG', 'CONGO, Peoples Republic of', 'OK'),
('COK', 'COOK ISLANDS', 'OK'),
('COL', 'COLOMBIA', 'OK'),
('COM', 'COMOROS', 'OK'),
('CPV', 'CAPE VERDE', 'OK'),
('CRI', 'COSTA RICA', 'OK'),
('CUB', 'CUBA', 'OK'),
('CXR', 'CHRISTMAS ISLAND', 'OK'),
('CYM', 'CAYMAN ISLANDS', 'OK'),
('CYP', 'CYPRUS', 'OK'),
('CZE', 'CZECH REPUBLIC', 'OK'),
('DEU', 'GERMANY', 'OK'),
('DJI', 'DJIBOUTI', 'OK'),
('DMA', 'DOMINICA', 'OK'),
('DNK', 'DENMARK', 'OK'),
('DOM', 'DOMINICAN REPUBLIC', 'OK'),
('DZA', 'ALGERIA', 'OK'),
('ECU', 'ECUADOR', 'OK'),
('EGY', 'EGYPT', 'OK'),
('ERI', 'ERITREA', 'OK'),
('ESH', 'WESTERN SAHARA', 'OK'),
('ESP', 'SPAIN', 'OK'),
('EST', 'ESTONIA', 'OK'),
('ETH', 'ETHIOPIA', 'OK'),
('FIN', 'FINLAND', 'OK'),
('FJI', 'FIJI', 'OK'),
('FLK', 'FALKLAND ISLANDS (MALVINAS)', 'OK'),
('FRA', 'FRANCE', 'OK'),
('FRO', 'FAROE ISLANDS', 'OK'),
('FSM', 'MICRONESIA, FEDERATED STATES OF', 'OK'),
('FXX', 'FRANCE, METROPOLITAN', 'OK'),
('GAB', 'GABON', 'OK'),
('GBR', 'UNITED KINGDOM', 'OK'),
('GEO', 'GEORGIA', 'OK'),
('GGB', 'GUERNSEY CHANNEL ISLAND', 'OK'),
('GHA', 'GHANA', 'OK'),
('GIB', 'GIBRALTAR', 'OK'),
('GIN', 'GUINEA', 'OK'),
('GLP', 'GUADELOUPE', 'OK'),
('GMB', 'GAMBIA', 'OK'),
('GNB', 'GUINEA-BISSAU', 'OK'),
('GNQ', 'EQUATORIAL GUINEA', 'OK'),
('GRC', 'GREECE', 'OK'),
('GRD', 'GRENADA', 'OK'),
('GRL', 'GREENLAND', 'OK'),
('GTM', 'GUATEMALA', 'OK'),
('GUF', 'FRENCH GUIANA', 'OK'),
('GUM', 'GUAM', 'OK'),
('GUY', 'GUYANA', 'OK'),
('HKG', 'HONG KONG', 'OK'),
('HMD', 'HEARD AND MC DONALD ISLANDS', 'OK'),
('HND', 'HONDURAS', 'OK'),
('HRV', 'CROATIA (local name: Hrvatska)', 'OK'),
('HTI', 'HAITI', 'OK'),
('HUN', 'HUNGARY', 'OK'),
('IDN', 'INDONESIA', 'OK'),
('IND', 'INDIA', 'OK'),
('IOT', 'BRITISH INDIAN OCEAN TERRITORY', 'OK'),
('IRL', 'IRELAND', 'OK'),
('IRN', 'IRAN (ISLAMIC REPUBLIC OF)', 'OK'),
('IRQ', 'IRAQ', 'OK'),
('ISL', 'ICELAND', 'OK'),
('ISR', 'ISRAEL', 'OK'),
('ITA', 'ITALY', 'OK'),
('JAM', 'JAMAICA', 'OK'),
('JGB', 'JERSEY CHANNEL ISLAND', 'OK'),
('JOR', 'JORDAN', 'OK'),
('JPN', 'JAPAN', 'OK'),
('KAZ', 'KAZAKHSTAN', 'OK'),
('KEN', 'KENYA', 'OK'),
('KGZ', 'KYRGYZSTAN', 'OK'),
('KHM', 'CAMBODIA', 'OK'),
('KIR', 'KIRIBATI', 'OK'),
('KNA', 'SAINT KITTS AND NEVIS', 'OK'),
('KOR', 'KOREA, REPUBLIC OF', 'OK'),
('KWT', 'KUWAIT', 'OK'),
('LAO', 'LAOS PEOPLES DEMOCRATIC REPUBLIC', 'OK'),
('LBN', 'LEBANON', 'OK'),
('LBR', 'LIBERIA', 'OK'),
('LBY', 'LIBYAN ARAB JAMAHIRIYA', 'OK'),
('LCA', 'SAINT LUCIA', 'OK'),
('LIE', 'LIECHTENSTEIN', 'OK'),
('LKA', 'SRI LANKA', 'OK'),
('LSO', 'LESOTHO', 'OK'),
('LTU', 'LITHUANIA', 'OK'),
('LUX', 'LUXEMBOURG', 'OK'),
('LVA', 'LATVIA', 'OK'),
('MAC', 'MACAU', 'OK'),
('MAR', 'MOROCCO', 'OK'),
('MCO', 'MONACO', 'OK'),
('MDA', 'MOLDOVA, REPUBLIC OF', 'OK'),
('MDG', 'MADAGASCAR', 'OK'),
('MDV', 'MALDIVES', 'OK'),
('MEX', 'MEXICO', 'OK'),
('MGB', 'ISLE OF MAN', 'OK'),
('MHL', 'MARSHALL ISLANDS', 'OK'),
('MKD', 'MACEDONIA, THE FORMER YUGOSLAV R', 'OK'),
('MLI', 'MALI', 'OK'),
('MLT', 'MALTA', 'OK'),
('MMR', 'MYANMAR', 'OK'),
('MNG', 'MONGOLIA', 'OK'),
('MNP', 'NORTHERN MARIANA ISLANDS', 'OK'),
('MOZ', 'MOZAMBIQUE', 'OK'),
('MRT', 'MAURITANIA', 'OK'),
('MSR', 'MONTSERRAT', 'OK'),
('MTQ', 'MARTINIQUE', 'OK'),
('MUS', 'MAURITIUS', 'OK'),
('MWI', 'MALAWI', 'OK'),
('MYS', 'MALAYSIA', 'OK'),
('MYT', 'MAYOTTE', 'OK'),
('NAM', 'NAMIBIA', 'OK'),
('NCL', 'NEW CALEDONIA', 'OK'),
('NER', 'NIGER', 'OK'),
('NFK', 'NORFOLK ISLAND', 'OK'),
('NGA', 'NIGERIA', 'OK'),
('NIC', 'NICARAGUA', 'OK'),
('NIU', 'NIUE', 'OK'),
('NLD', 'NETHERLANDS', 'OK'),
('NONE', 'No valid country assigned', 'OK'),
('NOR', 'NORWAY', 'OK'),
('NPL', 'NEPAL', 'OK'),
('NRU', 'NAURU', 'OK'),
('NZL', 'NEW ZEALAND', 'OK'),
('OMN', 'OMAN', 'OK'),
('PAK', 'PAKISTAN', 'OK'),
('PAN', 'PANAMA', 'OK'),
('PCN', 'PITCAIRN', 'OK'),
('PER', 'PERU', 'OK'),
('PHL', 'PHILIPPINES', 'OK'),
('PLW', 'PALAU', 'OK'),
('PNG', 'PAPUA NEW GUINEA', 'OK'),
('POL', 'POLAND', 'OK'),
('PRI', 'PUERTO RICO', 'OK'),
('PRK', 'KOREA, DEMOCRATIC PEOPLES REPUBL', 'OK'),
('PRT', 'PORTUGAL', 'OK'),
('PRY', 'PARAGUAY', 'OK'),
('PSE', 'PALESTINIAN TERRITORY, Occupied', 'OK'),
('PYF', 'FRENCH POLYNESIA', 'OK'),
('QAT', 'QATAR', 'OK'),
('REU', 'REUNION', 'OK'),
('ROU', 'ROMANIA', 'OK'),
('RUS', 'RUSSIAN FEDERATION', 'OK'),
('RWA', 'RWANDA', 'OK'),
('SAU', 'SAUDI ARABIA', 'OK'),
('SDN', 'SUDAN', 'OK'),
('SEN', 'SENEGAL', 'OK'),
('SGP', 'SINGAPORE', 'OK'),
('SGS', 'SOUTH GEORGIA AND THE SOUTH SAND', 'OK'),
('SHN', 'ST. HELENA', 'OK'),
('SJM', 'SVALBARD AND JAN MAYEN ISLANDS', 'OK'),
('SLB', 'SOLOMON ISLANDS', 'OK'),
('SLE', 'SIERRA LEONE', 'OK'),
('SLV', 'EL SALVADOR', 'OK'),
('SMR', 'SAN MARINO', 'OK'),
('SOM', 'SOMALIA', 'OK'),
('SPM', 'ST. PIERRE AND MIQUELON', 'OK'),
('STP', 'SAO TOME AND PRINCIPE', 'OK'),
('SUR', 'SURINAME', 'OK'),
('SVK', 'SLOVAKIA (Slovak Republic)', 'OK'),
('SVN', 'SLOVENIA', 'OK'),
('SWE', 'SWEDEN', 'OK'),
('SWZ', 'SWAZILAND', 'OK'),
('SYC', 'SEYCHELLES', 'OK'),
('SYR', 'SYRIAN ARAB REPUBLIC', 'OK'),
('TCA', 'TURKS AND CAICOS ISLANDS', 'OK'),
('TCD', 'CHAD', 'OK'),
('TGO', 'TOGO', 'OK'),
('THA', 'THAILAND', 'OK'),
('TJK', 'TAJIKISTAN', 'OK'),
('TKL', 'TOKELAU', 'OK'),
('TKM', 'TURKMENISTAN', 'OK'),
('TLS', 'EAST TIMOR', 'OK'),
('TON', 'TONGA', 'OK'),
('TTO', 'TRINIDAD AND TOBAGO', 'OK'),
('TUN', 'TUNISIA', 'OK'),
('TUR', 'TURKEY', 'OK'),
('TUV', 'TUVALU', 'OK'),
('TWN', 'TAIWAN', 'OK'),
('TZA', 'TANZANIA, UNITED REPUBLIC OF', 'OK'),
('UGA', 'UGANDA', 'OK'),
('UKR', 'UKRAINE', 'OK'),
('UMI', 'UNITED STATES MINOR OUTLYING ISL', 'OK'),
('URY', 'URUGUAY', 'OK'),
('USA', 'UNITED STATES', 'OK'),
('UZB', 'UZBEKISTAN', 'OK'),
('VAT', 'VATICAN CITY STATE (HOLY SEE)', 'OK'),
('VCT', 'SAINT VINCENT AND THE GRENADINES', 'OK'),
('VEN', 'VENEZUELA', 'OK'),
('VGB', 'VIRGIN ISLANDS (BRITISH)', 'OK'),
('VIR', 'VIRGIN ISLANDS (U.S.)', 'OK'),
('VNM', 'VIETNAM', 'OK'),
('VUT', 'VANUATU', 'OK'),
('WLF', 'WALLIS AND FUTUNA ISLANDS', 'OK'),
('WSM', 'SAMOA', 'OK'),
('YEM', 'YEMEN', 'OK'),
('YUG', 'YUGOSLAVIA', 'OK'),
('ZAF', 'SOUTH AFRICA', 'OK'),
('ZMB', 'ZAMBIA', 'OK'),
('ZWE', 'ZIMBABWE', 'OK');

CREATE TABLE IF NOT EXISTS `list_currency` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `alias` char(4) character set utf8 collate utf8_unicode_ci NOT NULL,
  `name` varchar(32) NOT NULL default '',
  `symbol` char(4) character set utf8 collate utf8_unicode_ci NOT NULL,
  `rank` int(10) unsigned NOT NULL default '0',
  `rate` double NOT NULL default '0',
  `symbol_text` char(8) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

INSERT INTO `list_currency` (`id`, `alias`, `name`, `symbol`, `rank`, `rate`, `symbol_text`) VALUES
(1, 'GBP', 'British Pound', ' &#1', 2, 0.71, '£'),
(2, 'USD', 'US Dollar', '&#36', 1, 1.4, '$'),
(3, 'ZAR', 'South African Rand', 'R', 4, 10, 'R'),
(4, 'DEU', 'Germany', '&#12', 10, 1, '€'),
(5, 'CHE', 'Swiss Franc', 'SFr', 12, 1, 'SFr'),
(6, 'FRA', 'France', '&#12', 8, 1, '€'),
(7, 'NLD', 'Netherlands', '&#12', 14, 1, '€'),
(8, 'EUR', 'Euro', '&#12', 3, 1, '€');

CREATE TABLE IF NOT EXISTS `list_frequency` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(64) NOT NULL,
  `freq_year` tinyint(1) unsigned NOT NULL default '0',
  `status` char(4) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

INSERT INTO `list_frequency` (`id`, `name`, `freq_year`, `status`) VALUES
(1, 'ANNUAL', 1, 'ok'),
(2, 'SEMI-ANN', 2, 'ok'),
(3, 'QUARTERLY', 4, 'ok'),
(4, 'MONTHLY', 12, 'ok'),
(5, 'None', 0, 'ok');

CREATE TABLE IF NOT EXISTS `list_rating` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(64) NOT NULL,
  `color_code` varchar(8) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=38 ;

INSERT INTO `list_rating` (`id`, `name`, `color_code`) VALUES
(1, 'No Opinion Formed', '#961FFF'),
(21, 'A ( Outstanding )', '#30FF19'),
(22, 'B ( Good )', '#F7FF08'),
(23, 'C (Average or Below )', '#FF0000');

CREATE TABLE IF NOT EXISTS `list_state` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `id_list_country` char(4) collate utf8_unicode_ci default NULL,
  `name` varchar(64) character set utf8 collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `id_list_country` (`id_list_country`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

INSERT INTO `list_state` (`id_list_country`, `name`) VALUES
('USA', 'Alabama'),
('USA', 'Alaska'),
('USA','Arizona'),
('USA','Arkansas'),
('USA','California'),
('USA','Colorado'),
('USA','Connecticut'),
('USA','Delaware'),
('USA','Florida'),
('USA','Georgia'),
('USA','Hawaii'),
('USA','Idaho'),
('USA','Illinois'),
('USA','Indiana'),
('USA','Iowa'),
('USA','Kansas'),
('USA','Kentucky'),
('USA','Louisiana'),
('USA','Maine'),
('USA','Maryland'),
('USA','Massachusetts'),
('USA','Michigan'),
('USA','Minnesota'),
('USA','Mississippi'),
('USA','Missouri'),
('USA','Montana'),
('USA','Nebraska'),
('USA','Nevada'),
('USA','New Hampshire'),
('USA','New Jersey'),
('USA','New Mexico'),
('USA','New York'),
('USA','North Carolina'),
('USA','North Dakota'),
('USA','Ohio'),
('USA','Oklahoma'),
('USA','Oregon'),
('USA','Pennsylvania'),
('USA','Rhode Island'),
('USA','South Carolina'),
('USA','South Dakota'),
('USA','Tennessee'),
('USA','Texas'),
('USA','Utah'),
('USA','Vermont'),
('USA','Virginia'),
('USA','Washington'),
('USA','West Virginia'),
('USA','Wisconsin'),
('USA','Wyoming'),

('USA','American Samoa'),
('USA','Guam'),
('USA','Northern Mariana Islands'),
('USA','Puerto Rico'),
('USA','U.S. Virgin Islands');

CREATE TABLE IF NOT EXISTS `tree_strategy` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `id_tree_strategy` int(10) unsigned NOT NULL,
  `alias` varchar(16) collate utf8_unicode_ci NOT NULL default '',
  `title` varchar(32) collate utf8_unicode_ci default '',
  `description` text collate utf8_unicode_ci,
  `rank` int(10) unsigned default NULL,
  `rank_end` int(10) unsigned default NULL,
  `rating_id` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `id_tree_strategy` (`id_tree_strategy`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=57 ;

INSERT INTO `tree_strategy` (`id`, `id_tree_strategy`, `alias`, `title`, `description`, `rank`, `rank_end`, `rating_id`) VALUES
(1, 44, 'Distressed_S', 'Distressed Debt', 'Distressed Debt strategies invest in, and may sell short, the securities of companies where the security''s price has been, or is expected to be, affected by a distressed situation. This may involve reorganizations, bankruptcies, distressed sales and other corporate restructurings. Depending on the manager''s style, investments may be made in bank debt, corporate debt, trade claims, common stock, preferred stock and warrants. Strategies may be sub-categorized as "high-yield" or "orphan equities." Leverage may be used by some managers. Fund managers may run a market hedge using S&P put options or put options spreads. Distressed  managers invest in non-investment grade debt. Objectives may range from high current income to acquisition of undervalued instruments. Emphasis is placed on assessing credit risk of the issuer. Some of the available high-yield instruments include extendible/reset securities, increasing-rate notes, pay-in-kind securities, step-up coupon securities, split-coupon securities and usable bonds.\n', 21, 21, 13),
(2, 44, 'Convertible_', 'Convertible Arbitrage', 'Convertible Arbitrage involves purchasing a portfolio of convertible securities, generally convertible bonds, and hedging a portion of the equity risk by selling short the underlying common stock. Certain managers may also seek to hedge interest rate exposure under some circumstances. Most managers employ some degree of leverage, ranging from zero to 6:1. The equity hedge ratio may range from 30 to 100 percent. The average grade of bond in a typical portfolio is BB-, with individual ratings ranging from AA to CCC. However, as the default risk of the company is hedged by shorting the underlying common stock, the risk is considerably better than the rating of the unhedged bond indicates.\n', 25, 25, 13),
(3, 49, 'Emerging_Mar', 'Asia', 'Equity - Asia funds invest in securities of companies or the sovereign debt of developing or Asia or Pac-Rim countries. ', 10, 10, 13),
(4, 49, 'Emerging_Mar1', 'Europe', 'Equity - Europe funds invest in securities of companies or the sovereign debt of established or developing  countries throughout western and eastern Europe.', 12, 12, 13),
(5, 49, 'Emerging_Mar2', 'Emerging Markets', 'Emerging Markets funds invest in securities of companies or the sovereign debt of developing or "emerging" countries.  This entails investing in emerging markets anywhere in the world, with no specific regional focus. Global funds will shift their weightings among regions according to market conditions and manager perspectives. ', 11, 11, 13),
(6, 49, 'Emerging_Mar3', 'Global', 'Equity - Global investing consists of a core holding of long equities hedged at all times with short sales of stocks and/or stock index options. Managers may invest in multiple markets/regions and across all sectors. Some managers maintain a substantial portion of assets within a hedged structure and commonly employ leverage. Where short sales are used, hedged assets may be comprised of an equal dollar value of long and short stock positions. Other variations use short sales unrelated to long holdings and/or puts on a major market index and put spreads. Conservative funds mitigate market risk by maintaining market exposure from zero to 100 percent. Aggressive funds may magnify market risk by exceeding 100 percent exposure and, in some instances, maintain a short exposure. In addition to equities, some funds may have limited assets invested in other types of securities.', 13, 13, 13),
(7, 44, 'Equity_Hedge', 'Long/Short', 'Credit - Long/Short investing consists of a core holding of long bonds hedged at all times with synthetic short sales of bonds and/or index options. Some managers maintain a substantial portion of assets within a hedged structure and commonly employ leverage. Where short sales are used, hedged assets may be comprised of an equal dollar value of long and short bond positions. \n', 17, 17, 13),
(8, 48, 'Equity_Marke', 'US Market Neutral', 'Equity US Market Neutral investing seeks to profit by exploiting pricing inefficiencies between related equity securities, neutralizing exposure to market risk by combining long and short positions on companies within the United States. One example of this strategy is to build portfolios made up of long positions in the strongest companies in several industries and taking corresponding short positions in those showing signs of weakness.\n', 7, 7, 13),
(9, 48, 'Equity_Marke1', 'Statistical Arbitrage', 'Equity - Statistical Arbitrage utilizes quantitative analysis of technical factors to exploit pricing inefficiencies between related equity securities, neutralizing exposure to market risk by combining long and short positions. The strategy is based on quantitative models for selecting specific stocks with equal dollar amounts comprising the long and short sides of the portfolio. Portfolios are typically structured to be market, industry, sector, and dollar neutral.', 6, 6, 13),
(10, 48, 'Eequity_Non-', 'Long Only', 'Equity - Long Only are predominately long equities although they have the ability to hedge with short sales of stocks and/or stock index options. These funds are commonly known as "stock-pickers, or fundamental".', 4, 4, 13),
(11, 40, 'Macro_-_Syst', 'Macro - Systematic', 'Macro - Systematic usually employs a proprietary, sometimes automated, trading system in order to benefit from arbitrage opportunities and price discrepancies involves investing by making leveraged bets on anticipated price movements of stock markets, interest rates, foreign exchange and physical commodities.  Most managers rely heavily on technical analysis and attempt to predict future movements (both long and short term) in order to benefit from shifts in value.', 32, 32, 13),
(12, 48, 'Event_Driven', 'Event Driven', 'Event-Driven is also known as "corporate life cycle" investing. This involves investing in opportunities created by significant transactional events, such as spin-offs, mergers and acquisitions, bankruptcy reorganizations, recapitalizations and share buybacks. The portfolio of some Event-Driven managers may shift in majority weighting between Risk Arbitrage and Distressed Securities, while others may take a broader scope. Instruments include long and short common and preferred stocks, as well as debt securities and options. Leverage may be used by some managers. Fund managers may hedge against market risk by purchasing S&P put options or put option spreads.\n', 8, 8, 13),
(13, 56, 'Activist', 'Activist', 'The techniques an activist manager may use range from talking to management to publicly advocating for change to campaigning to replace management. Funds that focus on corporate governance often will advocate for the nullification or removal of provisions such as poison\r\npills and staggered voting for directors by seeking board resolutions or amendments to a company''s governing documents or agreements. Unlike private equity funds and other kinds of "takeover" investors, activist strategies usually do not seek to control a company or necessarily hold an investment for more than a few years.', 39, 40, 13),
(14, 44, 'Fixed_Income', 'Direct Lending', 'Fixed Income - Direct Lending and Origination Fund strategies seek to leverage origination banking capabilities and asset management capabilities to capture attractive risk adjusted returns within direct lending opportunities The strategy seeks to capitalize on the systemic dislocation within the non-bank credit markets by providing secure cash flow senior and mezzanine financing to companies. ', 18, 18, 13),
(15, 44, 'Fixed_Income5', 'Leveraged Loans', 'Fixed Income - Leveraged Loans seeks to capitalize on providing leveraged loans for companies or individuals with less than A credit ratings or to finance high volatility transactions. The debt tends to have higher interest rates than typical loans. These rates reflect the higher level of risk involved in issuing the loan. In business, leveraged loans are also used in the leveraged buy-outs (LBOs) of other companies.', 20, 20, 13),
(16, 44, 'Fixed_Income2', 'Specialty Finance', 'Fixed Income - Specialty funds may invest in a variety of fixed income strategies. While many invest in multiple strategies, others may focus on a single strategy less followed by most fixed income hedge funds. Areas of focus include municipal bonds, corporate bonds, and global fixed income securities.\n', 23, 23, 13),
(17, 44, 'Fixed_Income6', 'Emerging Markets', 'Fixed Income - Emerging Markets is an actively managed portfolio of debt instruments issued by entities domiciled in Emerging Countries as defined by the World Bank.', 19, 19, 13),
(18, 44, 'Fixed_Income4', 'Structured Credit', 'Fixed Income - Structured Credit: ABS/RMBS/CMBS funds invest in mortgage-backed securities. Many funds focus solely on AAA-rated bonds but will as conditions permit pursue distressed opportunities. Instruments include: government agency, government-sponsored enterprise, private-label fixed- or adjustable-rate mortgage pass-through securities, fixed- or adjustable-rate collateralized mortgage obligations (CMOs), real estate mortgage investment conduits (REMICs) and stripped mortgage-backed securities (SMBSs). Funds may look to capitalize on security-specific mispricings. Hedging of prepayment risk and interest rate risk is common. Leverage may be used, as well as futures, short sales and options.\n', 24, 24, 13),
(19, 41, 'Fund_of_Fund3', 'Long Only', 'Invests in underlying long only accounts and funds of multiple styles and market caps.', 36, 36, 13),
(20, 40, 'Macro', 'Macro - Discretionary', 'Macro - Discretionary involves investing by making leveraged bets on anticipated price movements of stock markets, interest rates, foreign exchange and physical commodities. Macro managers employ a "top-down" global approach, and may invest in any markets using any instruments to participate in expected market movements. These movements may result from forecasted shifts in world economies, political fortunes or global supply and demand for resources, both physical and financial. Exchange-traded and over-the-counter derivatives are often used to magnify these price movements.\n', 31, 31, 13),
(21, 56, 'Managed_Futu', 'Managed Futures/CTA', 'Managed Futures, otherwise known as â€œcommodity trading advisors" (CTAs), manage client assets on a discretionary basis, using global futures markets (government securities, futures contracts and options on futures contracts ) as an investment medium. CTAs generally manage their clients'' assets using a proprietary trading system or a discretionary method that may involve long/short investments in futures contracts. Typical areas of focus include metals (gold, silver), grains (soybeans, corn, wheat), equity indexes (S&P futures, Dow futures, NASDAQ 100 futures) and soft commodities (cotton, cocoa, coffee, sugar), as well as foreign currency and U.S government bond futures. As an asset class, managed futures are known to be inversely correlated with stocks and bonds.', 41, 41, 13),
(22, 48, 'Merger_Arbit', 'Merger Arbitrage/Risk Arbitrage', 'Merger Arbitrage, sometimes called Risk Arbitrage, involves investment in event-driven situations such as leveraged buy-outs, mergers and hostile takeovers. Normally, the stock of an acquisition target appreciates while the acquiring company''s stock decreases in value. These strategies generate returns by purchasing stock of the company being acquired, and in some instances, selling short the stock of the acquiring company. Managers may employ the use of equity options as a low-risk alternative to the outright purchase or sale of common stock. Most Merger Arbitrage funds hedge against market risk by purchasing S&P put options or put option spreads.', 15, 15, 13),
(23, 56, 'Private_Equi', 'Private Equity', '', 44, 44, 13),
(24, 56, 'PIPES', 'PIPES', 'A private investment firm''s, mutual fund''s or other qualified investors'' purchase of stock in a company at a discount to the current market value per share for the purpose of raising capital. There are two main types of PIPEs - traditional and structured. A traditional PIPE is one in which stock, either common or preferred, is issued at a set price to raise capital for the issuer. A structured PIPE, on the other hand, issues convertible debt (common or preferred shares).', 43, 43, 13),
(25, 44, 'Relative_Val', 'Relative Value', 'Fixed Income - Relative Value attempts to take advantage of relative pricing discrepancies between different tranches of corporate debt. Managers may use mathematical, fundamental, or technical analysis to determine misvaluations. Securities may be mispriced relative to the underlying security, related securities, groups of securities, or the overall market. Many funds use leverage and seek opportunities globally. Arbitrage strategies include dividend arbitrage, pairs trading, options arbitrage and yield curve trading.', 22, 22, 13),
(26, 40, 'Sector_Energ', 'Commodities', 'Commodities / Energy is a strategy that focuses on investment within the energy sector. Investments can be long and short in various instruments with funds either diversified across the entire sector or specializing within a sub-sector, i.e., oil field service. It may also include the using global futures markets (futures contracts and options on futures contracts ) as an investment medium. Typical areas of focus include oil and gas futures as well as more exotic Crack & Dark Spreads, and HDD or CDD contracts. As an asset class, managed futures are known to be inversely correlated with stocks and bonds.', 29, 30, 13),
(27, 48, 'Sector_Misce', 'Sector ', 'Equity - Sector funds invest in securities of companies primarily focused on miscellaneous but usually singular sectors of investments, such as financial, healthcare/biotech, information technology, consumer cyclical, utilities, retail, transports, or other discrete industrial or service verticals.', 14, 14, 13),
(28, 56, 'Sector_Real_', 'Real Estate', 'Real Estate involves investing in securities of real estate investment trusts (REITs) and other real estate companies. Some funds may also invest directly in real estate property.\r\n', 45, 45, 13),
(29, 48, 'Short_Sellin', 'Dedicated Short Bias', 'Short Selling involves the sale of a security not owned by the seller; a technique used to take advantage of an anticipated price decline. To effect a short sale, the seller borrows securities from a third party in order to make delivery to the purchaser. The seller returns the borrowed securities to the lender by purchasing the securities in the open market. If the seller can buy that stock back at a lower price, a profit results. If the price rises, however, a loss results. A short seller must generally pledge other securities or cash with the lender in an amount equal to the market price of the borrowed securities. This deposit may be increased or decreased in response to changes in the market price of the borrowed securities.', 3, 3, 13),
(30, 56, 'UNKNOWN', 'Unknown strategy', 'Strategy assigned when no know standard strategy', 52, 52, 13),
(31, 56, 'Multi-Strate', 'Multi-Strategy', 'Combination of hedge fund strategies', 42, 42, 13),
(32, 41, 'Fund_of_Fund', 'Hedge Fund', 'Invests in underlying hedge funds of multiple strategies.', 35, 35, 13),
(33, 42, 'Cash_Managem', 'Cash Management', 'General Cash Management Strategy - Cash, Money Markets, Repos, Treasuries with Maturity under 90 Days Only.', 49, 49, 13),
(34, 41, 'Fund_of_Fund2', 'Private Equity', 'Invests in underlying private equity funds of multiple strategies.', 37, 37, 13),
(35, 41, 'Fund_of_Hedg', 'Emerging Managers', 'Invests in an underlying pool of newly formed hedge funds', 34, 34, 13),
(36, 41, 'Seeder', 'Seeder', 'In the business of seeding newly created hedge funds', 38, 38, 13),
(37, 44, 'Catastrophe_', 'Catastrophe Insurance', '', 26, 26, 13),
(38, 56, 'NONE', 'No valid strategy assigned', '', 51, 51, 13),
(39, 26, 'Energy', 'Energy', '', 30, 30, 13),
(40, 56, 'Macro1', 'Macro', '', 28, 32, 13),
(41, 56, 'Fund_of_Fund4', 'Fund of Fund', 'Generalist Fund of Fund investing in HFs, PE, Seeding, etc...', 33, 38, 13),
(42, 56, 'Cash___Expen', 'Cash & Expenses', '', 48, 50, 13),
(43, 42, 'Liabilities', 'Liabilities', '', 50, 50, 13),
(44, 56, 'Credit', 'Fixed Income', '', 16, 27, 13),
(45, 48, 'Volatility', 'Volatility Arbitrage', '', 5, 5, 13),
(46, 56, 'Interest_Rat', 'Interest Rates', '', 53, 53, 13),
(47, 56, 'Duration', 'Duration', '', 54, 54, 13),
(48, 56, 'Equity', 'Equity', '', 1, 15, 13),
(49, 48, 'Geographic', 'Geographic', '', 9, 13, 13),
(50, 13, 'Life_Science', 'Life Sciences', '', 40, 40, 13),
(51, 48, 'Long/Short', 'Long/Short', '', 2, 2, 13),
(52, 56, 'Other_-_WTS,', 'Other - WTS, IOs, ILNs', '', 46, 46, 13),
(53, 44, 'ABS/MBS', 'ABS/MBS', '', 27, 27, 13),
(54, 56, 'Mutual_Fund', 'Mutual Fund', '', 47, 47, 13),
(55, 56, 'BRIAN', 'BRIAN', '', 55, 55, 13),
(56, 56, 'root', 'root', '', 0, 0, 0);

CREATE TABLE IF NOT EXISTS `vehicle` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `id_vehicle_entity` int(11) unsigned NOT NULL,
  `id_vehicle_type` int(11) unsigned default NULL,
  `id_admin_method` int(11) unsigned NOT NULL,
  `id_tree_strategy` int(11) unsigned default NULL,
  `id_list_currency` int(10) unsigned NOT NULL,
  `name` varchar(256) collate utf8_unicode_ci NOT NULL,
  `alias` varchar(256) collate utf8_unicode_ci default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_vehicle_entity` (`id_vehicle_entity`),
  KEY `id_vehicle_type` (`id_vehicle_type`),
  KEY `id_admin_method` (`id_admin_method`),
  KEY `fk_vehicle_strategy` (`id_tree_strategy`),
  KEY `id_list_currency` (`id_list_currency`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `vehicle_category` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(64) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

INSERT INTO `vehicle_category` (`id`, `name`) VALUES
(1, 'Hedge Fund'),
(2, 'Fund of Hedge Funds'),
(3, 'Private Equity'),
(4, 'Fund of Private Equity'),
(5, 'Private REIT'),
(6, 'Separately Managed Vehicle'),
(7, '*Other'),
(8, '*Unknown');

CREATE TABLE IF NOT EXISTS `vehicle_entity` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `id_company` int(11) unsigned NOT NULL,
  `id_list_country` char(4) NOT NULL,
  `id_list_state` int(11) unsigned default NULL,
  `id_list_frequency` int(11) unsigned default NULL,
  `id_list_role` int(11) unsigned default NULL,
  `id_vehicle_entity_type` int(10) unsigned default NULL,
  `name` varchar(256) NOT NULL,
  `alias` varchar(64) default NULL,
  `fiscal_end` date default NULL,
  `status` varchar(16) default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_vehicle_entity_1` (`id_company`),
  KEY `fk_vehicle_entity_2` (`id_list_country`),
  KEY `list_state_id` (`id_list_state`),
  KEY `list_frequency_id` (`id_list_frequency`),
  KEY `list_role_id` (`id_list_role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `vehicle_entity_type` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(64) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

INSERT INTO `vehicle_entity_type` (`id`, `name`) VALUES
(1, 'Corp - Offshore'),
(2, 'Corp - US'),
(3, 'LLC - US'),
(4, 'LP - US - 3c1'),
(5, 'LP - US - 3c7'),
(6, 'LP -Offshore'),
(7, 'Managed Account'),
(8, 'Master Limited Partnership (MLP)'),
(9, 'Mutual Fund'),
(10, 'RIC - Offshore'),
(11, 'RIC - US'),
(12, 'Segregate Portfolio (SPC)'),
(13, 'SICAV'),
(14, 'UCIT'),
(15, 'Unit Trust'),
(16, '*Other');

CREATE TABLE IF NOT EXISTS `vehicle_status` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(16) character set utf8 collate utf8_unicode_ci NOT NULL default '',
  `rank` int(10) unsigned NOT NULL default '0',
  `description` varchar(255) NOT NULL default '',
  `color` varchar(8) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

INSERT INTO `vehicle_status` (`id`, `name`, `rank`, `description`, `color`) VALUES
(1, 'IGNORE', 10, 'Fund ignored', '#000000'),
(2, 'WATCH - TIER 1', 2, 'Watch - Tier 1', ''),
(3, 'INVESTED', 1, 'Fund held in portfolios', '#009HEAD900'),
(4, 'PORTFOLIO', 100, 'Internal portfolio of funds', '#000000'),
(5, 'PRE-TRADE DD', 5, 'Pre Trade DD', ''),
(6, 'WATCH - TIER 2', 3, 'Watch - Tier 2', ''),
(7, 'WATCH - TIER 3', 4, 'Watch - Tier 3', ''),
(8, 'SMP - Tier 1', 11, 'Great Team; Strategy in Favor', ''),
(9, 'SMP - Tier 2', 12, 'Great Team; Low Strategy Demand', ''),
(10, 'SMP - Tier 3', 13, 'Solid (Above Average) Team', ''),
(11, 'SMP - Tier 4', 14, 'Undistinguished', ''),
(12, 'SMP - Tier 5', 15, 'Good, but “hairy” (i.e., Manager has personal issues; Manager came from Galleon, etc.). ', ''),
(13, 'TBAccts', 10, 'Active Trial Balance Accts', '#000000');

CREATE TABLE IF NOT EXISTS `vehicle_type` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(64) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

INSERT INTO `vehicle_type` (`id`, `name`) VALUES
(1, 'Stand-Alone'),
(2, 'Master/Feeder'),
(3, 'Parallel'),
(4, '*Other'),
(5, '*Unknown');

ALTER TABLE `address`
  ADD CONSTRAINT `address_ibfk_1` FOREIGN KEY (`id_list_state`) REFERENCES `list_state` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `address_ibfk_2` FOREIGN KEY (`id_list_country`) REFERENCES `list_country` (`id`) ON UPDATE CASCADE;

ALTER TABLE `company`
  ADD CONSTRAINT `company_ibfk_1` FOREIGN KEY (`id_address`) REFERENCES `address` (`id`) ON UPDATE CASCADE;

ALTER TABLE `tree_strategy`
ADD CONSTRAINT `tree_strategy_ibfk_1` FOREIGN KEY (`id_tree_strategy`) REFERENCES `tree_strategy` (`id`) ON UPDATE CASCADE;

ALTER TABLE `list_state`

ADD CONSTRAINT `list_state_ibfk_1` FOREIGN KEY (`id_list_country`) REFERENCES `list_country` (`id`) ON UPDATE CASCADE;

ALTER TABLE `vehicle`
  ADD CONSTRAINT `vehicle_ibfk_1` FOREIGN KEY (`id_vehicle_entity`) REFERENCES `vehicle_entity` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `vehicle_ibfk_2` FOREIGN KEY (`id_vehicle_type`) REFERENCES `vehicle_type` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `vehicle_ibfk_3` FOREIGN KEY (`id_admin_method`) REFERENCES `admin_method` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `vehicle_ibfk_4` FOREIGN KEY (`id_tree_strategy`) REFERENCES `tree_strategy` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `vehicle_ibfk_5` FOREIGN KEY (`id_list_currency`) REFERENCES `list_currency` (`id`) ON UPDATE CASCADE;

GRANT usage ON pcx.* TO 'pcxUser'@'localhost';
DROP USER 'pcxUser'@'localhost';
CREATE USER 'pcxUser'@'localhost' IDENTIFIED BY 'pcxPwd';
GRANT ALL PRIVILEGES ON pcx.* TO 'pcxUser'@'localhost';
FLUSH PRIVILEGES;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
