<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
  /**
   * @var string the default layout for the controller view. Defaults to 'column1',
   * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
   */
  public $layout = 'column1';
  /**
   * @var array context menu items. This property will be assigned to {@link CMenu::items}.
   */
  public $menu = array();
  /**
   * @var array the breadcrumbs of the current page. The value of this property will
   * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
   * for more details on how to specify this property.
   */
  public $breadcrumbs = array();

  /*
   * we have this special handling to get the REST POST request*/
  public $requestBody;

  public function requestBody(){ //Fetch input from Json, put it in a class attribute
    $this->requestBody = json_decode(file_get_contents('php://input'), true);
    if (($this->requestBody == false)) {
      $msg = 'request body is empty. ';
      $this->_sendResponse(500, sprintf($msg));
    }
  }

  //This is the core endpoint. Every request must end through this function.
  // $errorData should be the submitted data from the backed plus session data
  protected function _sendResponse($status = 200, $body = '',  $content_type = 'text/html'){
    //@TODO add $error data as an argument and update all calling functions. Default to empty (for Successful calls)
    $errorData='';

    //@TODO log for Due Diligence. Future improvement, have to ask what is to be stored.
    $this->_logDueDiligence();

    if ($status != 200)
      $this->_logError($status, $body, $errorData);

    // set the status
    $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
    header($status_header);
    // and the content type
    header('Content-type: ' . $content_type);

    //create a nice body if empty
    echo ($body != '')? $body : $this->_buildResponseBody($status);
    Yii::app()->end();
  }

  private function _logDueDiligence(){

  }

  private function _logError($status, $body = '', $errorData){

  }

  private function _buildResponseBody($status){
    $message = '';
    // this is purely optional, but makes the pages a little nicer to read
    // for your users.  Since you won't likely send a lot of different status codes,
    // this also shouldn't be too ponderous to maintain
    switch ($status) {
      case 400:
        $message = 'The request cannot be fulfilled due to bad syntax.';
        break;
      case 401:
        $message = 'You must be authorized to view this page.';
        break;
      case 404:
        $message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
        break;
      case 500:
        $message = 'The server encountered an error processing your request.';
        break;
      case 501:
        $message = 'The requested method is not implemented.';
        break;
    }

    // servers don't always have a signature turned on
    // (this is an apache directive "ServerSignature On")
    $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];

    // this should be templated in a real-world solution
    $body = '
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title>' . $status . ' ' . $this->_getStatusCodeMessage($status) . '</title>
</head>
<body>
    <h1>' . $this->_getStatusCodeMessage($status) . '</h1>
    <p>' . $message . '</p>
    <hr />
    <address>' . $signature . '</address>
</body>
</html>';

    return $body;
  }

  protected function _saveModelError($model)
  {
    $msg = "<h1>Error</h1>";
    $msg .= sprintf("Couldn't create model <b>%s</b>", get_class($model));
    $msg .= "<ul>";
    foreach ($model->errors as $attribute => $attr_errors) {
      $msg .= "<li>Attribute: $attribute</li>";
      $msg .= "<ul>";
      foreach ($attr_errors as $attr_error)
        $msg .= "<li>$attr_error</li>";
      $msg .= "</ul>";
    }
    $msg .= "</ul>";
    $this->_sendResponse(400, $msg);
  }

  private function _getStatusCodeMessage($status)
  {
    // these could be stored in a .ini file and loaded
    // via parse_ini_file()... however, this will suffice
    // for an example
    $codes = Array(
      200 => 'OK',
      400 => 'Bad Request',
      401 => 'Unauthorized',
      402 => 'Payment Required',
      403 => 'Forbidden',
      404 => 'Not Found',
      500 => 'Internal Server Error',
      501 => 'Not Implemented',
    );
    return (isset($codes[$status])) ? $codes[$status] : '';
  }

  /*
   * Also, in all REST methods where an authentication is required, we need to put
  $this->_checkAuth(); at the beginning of each method.
  The API user then needs to set the X_USERNAME and X_PASSWORD headers in his request.
   */

  private function _checkAuth()
  {
    // Check if we have the USERNAME and PASSWORD HTTP headers set?
    if (!(isset($_SERVER['HTTP_X_USERNAME']) and isset($_SERVER['HTTP_X_PASSWORD']))) {
      // Error: Unauthorized
      $this->_sendResponse(401);
    }
    $username = $_SERVER['HTTP_X_USERNAME'];
    $password = $_SERVER['HTTP_X_PASSWORD'];
    // Find the user
    $user = User::model()->find('LOWER(username)=?', array(strtolower($username)));
    if ($user === null) {
      // Error: Unauthorized
      $this->_sendResponse(401, 'Error: User Name is invalid');
    } else if (!$user->validatePassword($password)) {
      // Error: Unauthorized
      $this->_sendResponse(401, 'Error: User Password is invalid');
    }
  }
}
