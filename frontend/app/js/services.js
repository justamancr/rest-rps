'use strict';

/* Services */

angular.module('phonecatServices', ['ngResource']).
    factory('Phone', function($resource){
      // This is the original READ FROM LOCAL FILE
        // return $resource('phones/:phoneId.json', {}, {
      // This was the angular tutorial TEST moved to a REST service
        //return $resource('/varios/RPS/backend/rps/rps/index.php/api/championship/test', {}, {
      //This is me, replacing the tutorial structure with the Challenge one
        return $resource('/varios/RPS/backend/rps/rps/index.php/api/championship/top', {}, {

        //query: {method:'GET', params:{phoneId:'phones'}, isArray:true}
        query: {method:'GET', isArray:false}
  });
});


