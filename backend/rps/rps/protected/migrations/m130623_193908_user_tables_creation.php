<?php

class m130623_193908_user_tables_creation extends CDbMigration
{
	public function up()
	{
    $this->createTable('user', array(
      'id'=>'int(11) NOT NULL AUTO_INCREMENT',
      'name'=>'varchar(64) NOT NULL',
      'access'=>'varchar(16) NOT NULL',
      'password'=>'varchar(32) NOT NULL',
      'email'=>'varchar(128) NOT NULL',
      'pwd_date'=>'date NOT NULL',
      'login_fail'=>'int(10) unsigned NOT NULL',
      'status'=>'varchar(16) NOT NULL',
      'email_token'=>'varchar(64) NOT NULL',
      'PRIMARY KEY (id)',
    ),  'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1');     
	}

	public function down()
	{
		$this->dropTable('user');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
