<?php

class SiteController extends Controller
{
	public $layout='column1';

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$headers="From: {$model->email}\r\nReply-To: {$model->email}";
				mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

    public function actionLogin() {

        Yii::import('ext.eoauth.*');

        $ui = new EOAuthUserIdentity(
            array(
                //Set the "scope" to the service you want to use
                'scope'=>'https://sandbox.google.com/apis/ads/publisher/',
                'provider'=>array(
                    'request'=>'https://www.google.com/accounts/OAuthGetRequestToken',
                    'authorize'=>'https://www.google.com/accounts/OAuthAuthorizeToken',
                    'access'=>'https://www.google.com/accounts/OAuthGetAccessToken',
                )
            )
        );

        if ($ui->authenticate()) {
            $user=Yii::app()->user;
            $user->login($ui);
            $this->redirect($user->returnUrl);
        }
        else throw new CHttpException(401, $ui->error);

    }



    public function actionLogout() {

        Yii::app()->user->logout();

        // Redirect to application home page.
        $this->redirect(Yii::app()->homeUrl);
    }
}
