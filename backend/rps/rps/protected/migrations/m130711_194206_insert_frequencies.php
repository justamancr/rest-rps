<?php

class m130711_194206_insert_frequencies extends CDbMigration
{
  public function up(){
    $this->truncateTable('list_frequency');
    $this->dropColumn('list_frequency','status');
    $this->renameColumn('list_frequency','freq_year','days');

    $this->insert('list_frequency', array('name'=>'Daily','days' => 1) );
    $this->insert('list_frequency', array('name'=>'Weekly','days' => 7) );
    $this->insert('list_frequency', array('name'=>'Monthly','days' => 30) );
    $this->insert('list_frequency', array('name'=>'Quarterly','days' => 91) );
    $this->insert('list_frequency', array('name'=>'Semi-Monthly','days' => 60) );
    $this->insert('list_frequency', array('name'=>'Semi-Annual','days' => 182) );
    $this->insert('list_frequency', array('name'=>'Annual','days' => 365) );

    $this->insert('list_frequency', array('name'=>'35 Days','days' => 35) );
    $this->insert('list_frequency', array('name'=>'45 Days','days' => 45) );
    $this->insert('list_frequency', array('name'=>'60 Days','days' => 60) );
    $this->insert('list_frequency', array('name'=>'75 Days','days' => 75) );
    $this->insert('list_frequency', array('name'=>'90 Days','days' => 90) );
    $this->insert('list_frequency', array('name'=>'95 Days','days' => 95) );
    $this->insert('list_frequency', array('name'=>'100 Days','days' => 100) );
    $this->insert('list_frequency', array('name'=>'120 Days','days' => 120) );

    $this->insert('list_frequency', array('name'=>'*Other','days' => 0) );
    $this->insert('list_frequency', array('name'=>'*Unknown','days' => 0) );

  }

  public function down(){
    $this->truncateTable('list_frequency');
    $this->addColumn('list_frequency','status','char(2) not null');
    $this->renameColumn('list_frequency','days','freq_year');
  }

  /*
  // Use safeUp/safeDown to do migration with transaction
  public function safeUp()
  {
  }

  public function safeDown()
  {
  }
  */
}
