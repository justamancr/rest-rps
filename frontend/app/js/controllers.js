'use strict';

/* Controllers */

function PhoneListCtrl($scope, Phone) {
    $scope.phones = Phone.query();
    $scope.orderProp = 'age';
    //alert(JSON.stringify($scope.phones, null, 4));
    // alert($scope.phones);
}

//PhoneListCtrl.$inject = ['$scope', 'Phone'];



function PhoneDetailCtrl($scope, $routeParams, Phone) {
    $scope.phone = Phone.get({phoneId: $routeParams.phoneId}, function(phone) {
        $scope.mainImageUrl = phone.images[0];
    });

    $scope.setImage = function(imageUrl) {
        $scope.mainImageUrl = imageUrl;
    }
}

//PhoneDetailCtrl.$inject = ['$scope', '$routeParams', 'Phone'];

function MyController($scope, $http) {
    //var championship = '[ [ [ ["Armando", "P"], ["Dave", "S"] ], [ ["Richard", "R"], ["Michael", "S"] ] ], [ [ ["Allen", "S"], ["Omer", "P"] ], [ ["John", "R"], ["Robert", "P"] ] ] ]';
    var championship = document.getElementById('championship').value;
    $scope.myData = {};
    $scope.myData.doClick = function(item, event) {
        var getResponse= $http({
          method: 'POST',
          url: '/varios/RPS/backend/rps/rps/index.php/api/championship/new',
          data: championship,
          headers: {'Content-Type': 'application/json'}
        });
        getResponse.success(function(data, status, headers, config) {
            $scope.myData.fromServer = "SUCCESS!";
        });
        getResponse.error(function(data, status, headers, config) {
            alert("FAILED! are you doing cross domain posting? frontend and backend should be in the same domain. Please also remember you need to input a file from the index of this app.");
        });
    }
}