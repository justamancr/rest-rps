<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
  'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
  'name'=>'My Console Application',
  
  //This connection is used by migrations
  'components' => array(
    'db'=>array(
            'class' => 'CDbConnection',
            'connectionString' =>
            'mysql:host=localhost;dbname=pcx',
            'emulatePrepare' => true,
            'username' => 'pcxUser',
            'password' => 'pcxPwd',
            'charset' => 'utf8',
    ),
    'previewDB'=>array(
            'class' => 'CDbConnection',
            'connectionString' =>
            'mysql:host=localhost;dbname=pcx_preview',
            'emulatePrepare' => true,
            'username' => 'pcxUserPreview',
            'password' => 'pcxPreviewPwd',
            'charset' => 'utf8',
    ),
  ),
);
