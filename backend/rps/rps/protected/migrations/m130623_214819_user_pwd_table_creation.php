<?php

class m130623_214819_user_pwd_table_creation extends CDbMigration
{
	public function up()
	{
    //Table creation
    $this->createTable('user_pwd', array(
      'id'=>'int(11) NOT NULL AUTO_INCREMENT',
      'user_id'=>'int(11) NOT NULL',
      'password'=>'varchar(32) NOT NULL',
      'PRIMARY KEY (id)',
      'UNIQUE KEY `idx_user_password` (`user_id`,`password`)',
    ),  'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1');  
    
    //FK creation
    $this->addForeignKey('user_pwd_ibfk_1', 'user_pwd', 'user_id', 'user', 'id','CASCADE','CASCADE');   
      
	}

	public function down()
	{
    $this->dropForeignKey('user_pwd_ibfk_1', 'user_pwd');
    $this->dropTable('user_pwd');
  }

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
