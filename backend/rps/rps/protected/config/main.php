<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
$mainConfig = array(
  'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
  'name' => 'Yii Blog Demo',

  // preloading 'log' component
  'preload' => array('log'),

  // autoloading model and component classes
  'import' => array(
    'application.models.*',
    'application.components.*',
  ),

  'defaultController' => 'Rfundclass',

  'modules' => array(
    'gii' => array(
      'class' => 'system.gii.GiiModule',
      'password' => 'sisisi',
      // 'ipFilters'=>array(...a list of IPs...),
      // 'newFileMode'=>0666,
      // 'newDirMode'=>0777,
    ),
  ),
  // application components
  'components' => array(
    'user' => array(
      // enable cookie-based authentication
      'allowAutoLogin' => true,
    ),

    'errorHandler' => array(
      // use 'site/error' action to display errors
      'errorAction' => 'site/error',
    ),
    'urlManager' => array(
      'urlFormat' => 'path',
      'rules' => array(
        'gii' => 'gii',
        'gii/<controller:\w+>' => 'gii/<controller>',
        'gii/<controller:\w+>/<action:\w+>' => 'gii/<controller>/<action>',
        // REST patterns
        array('genapi/list', 'pattern' => 'genapi/<model:\w+>', 'verb' => 'GET'),
        array('genapi/view', 'pattern' => 'genapi/<model:\w+>/<id:\w+>', 'verb' => 'GET'),
        array('genapi/update', 'pattern' => 'genapi/<model:\w+>/<id:\d+>', 'verb' => 'PUT'),
        array('genapi/delete', 'pattern' => 'genapi/<model:\w+>/<id:\d+>', 'verb' => 'DELETE'),
        array('genapi/create', 'pattern' => 'genapi/<model:\w+>', 'verb' => 'POST'),
        // Championship API
        'api/<controller:\w+>/<action:\w+>' => '<controller>/<action>',
        // Other controllers
        '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
      ),
    ),
  ),
  // application-level parameters that can be accessed
  // using Yii::app()->params['paramName']
  'params' => require(dirname(__FILE__) . '/params.php'),
);

$mainConfig ['components']['db'] = array(
  'connectionString' =>'mysql:host=localhost;dbname=justaman_rps',
  'emulatePrepare' => true,
  'username' => 'justaman_multi',
  'password' => 'x5,rb*u;l,u{',
  'charset' => 'utf8',
  'tablePrefix' => 'tbl_',
);

$mainConfig ['components']['log'] = array(
  'class' => 'CLogRouter',
  'routes' => array(
    array(
      'class' => 'CFileLogRoute',
      'levels' => 'error, warning',
    ),
  ),
);

switch ($_SERVER['HTTP_HOST']) {
  case 'preview1.simplify-llc.com':
    $mainConfig ['components']['db']['connectionString'] = 'mysql:host=localhost;dbname=pcx_preview';
    $mainConfig ['components']['db']['username'] = 'pcxUserPreview';
    $mainConfig ['components']['db']['password'] = 'pcxPreviewPwd';
    break;
  case 'production.simplify-llc.com':

    break;
  default:
    $mainConfig ['components']['log']['routes'][]= array('class'=>'CWebLogRoute');
    break;
}
return $mainConfig;
