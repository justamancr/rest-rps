<!doctype html>
<html lang="en" ng-app="myapp">
<head>
  <meta charset="utf-8">
  <title>RPS, Angular + PHP REST server</title>
  <link rel="stylesheet" href="css/app.css">
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/simple.css" type="text/css" media="screen" />
  <script src="lib/angular/angular.js"></script>
  <script src="js/app.js"></script>
  <script src="js/controllers.js"></script>
  <script src="js/filters.js"></script>
  <script src="js/services.js"></script>
  <script src="lib/angular/angular-resource.js"></script>
</head>
<body>

<div id="contenido">
  <div id="contenidoInterno">
    <div id="encabezado">
      <div id="logo">
        <h1>RPS, Angular + PHP REST server</h1>
      </div>
      <div id="menu">
        <ul>
          <li><a href="#"></a></li>
        </ul>
      </div>
    </div>
    <!-- termina encabezado -->

    <div id="page">
      <div id="content">
        <h2>Uploading Championship...</h2>
        <?php
        if ($_FILES["file"]["error"] > 0) {
          echo "Error: " . $_FILES["file"]["error"] . "<br>";
        } else {
          echo "Upload: " . $_FILES["file"]["name"] . "<br>";
          echo "Type: " . $_FILES["file"]["type"] . "<br>";
          echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
          echo "Stored in: " . $_FILES["file"]["tmp_name"];
          ?>

          <?php
          $handle = file_get_contents($_FILES["file"]["tmp_name"]);
          echo "<div><h2>These are the file contents:</h2><strong>".$handle."</strong></div>";
          echo "<input type='hidden' id='championship' value='".$handle."'>";
        }
        ?>
        <div ng-controller="MyController" >
          <button ng-click="myData.doClick(item, $event)">POST to REST Service</button>
          <br/>
          Response: {{myData.fromServer}}
        </div>
      </div>
      <div id="sidebar">
        <ul>
          <li id="search">
          </li>
          <li>
            <h3>Usefull Links</h3>
            <ul>
              <li><a href=""><strong></strong></a></li>
            </ul>
          </li>
        </ul>
      </div>
      <!-- end #sidebar -->
      <div style="clear: both;">&nbsp;</div>
      <!-- end #content -->
      <div id="widebar">
        <div id="colA"></div>
        <div id="colB"></div>
        <div id="colC"></div>
        <div style="clear: both;">&nbsp;</div>
      </div>
      <!-- end #widebar -->
    </div>
    <!-- end #page -->
  </div>
  <!-- fin de #contenidoInterno -->
  <div id="footer">
    <p>Anthony Phillips. Justaman</a></p>
  </div>
</div>
</body>
</html>