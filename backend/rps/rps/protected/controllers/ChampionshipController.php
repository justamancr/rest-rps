<?php
/*
Apache Issues
If PUT or DELETE requests don't work in your Apache setup (perhaps you get an 403 - Forbidden error), you can put the following .htaccess file in the application's web root:
<Limit GET POST PUT DELETE>
    order deny,allow
allow from all
    </Limit>
  */


class ChampionshipController extends Controller
{
// Members
  /**
   * Key which has to be in HTTP USERNAME and PASSWORD headers
   */
  Const APPLICATION_ID = 'ASCCPE';

  /**
   * Default response format
   * either 'json' or 'xml'
   */
  private $format = 'json';
  /**
   * @return array action filters
   */
  public function filters()
  {
    return array();
  }



// Actions

/*
array (
      array (
        array (
          array ("Armando", "P"),
          array ("Dave", "S")
        ),
        array (
          array ("Richard", "R"),
          array ("Michael", "S")
        ),
      ),
      array (
        array (
          array ("Allen", "S"),
          array ("Omer", "P")
        ),
        array (
          array ("John", "R"),
          array ("Robert", "P")
        ),
      ),
    );

*/

/*
 * METHODS
 *
*/

  // This is method "a)" from the challenge
  // It takes a 2 element array, each element being an array with player name and strategy

  private function _rpsMatch($match){
    //a.1) if the number of players is not equal to 2, raise an exception
    if (count ($match) != 2){
      return (array("error","The data structure failed, the match received an even number of participants"));
    }
    $validInput = array ("R","P","S");

    $playerOneStrategy = strtoupper($match[0][1]);
    $playerTwoStrategy = strtoupper($match[1][1]);

    //a.2) if either player's strategy is something other than "R", "P" or "S" (case insensitive), raise an exception
    if ( !in_array($playerOneStrategy,$validInput) ||
      !in_array($playerTwoStrategy,$validInput)){
      return (array("error","The data structure failed, un valid option detected"));
    }

    //a.3) Otherwise, return the name and strategy of the winning player.
    //     If both players use the same strategy, the first player will be the winner.
    switch ($playerOneStrategy){
      case "R":
        $winner = $playerTwoStrategy == "P"?1:0;
        break;
      case "P":
        $winner = $playerTwoStrategy == "S"?1:0;
        break;
      case "S":
        $winner = $playerTwoStrategy == "R"?1:0;
        break;
    }
    return $match[$winner];
  }

  //This is method "b)" from the challenge
  //@Param $tournament: Takes an array with all the participants and their strategies, and returns a single array with name and strategy
  //@Param $returnSecondPlace: The main match sometimes has to return both, the winner and second place.
  // I guess a Singleton would be nice to catch the looser of the last match (second place also get points). Maybe later.
  // TODO: store first and second place in database with respective score.
  private function _rpsTournament($tournament, $returnSecondPlace=false){
    //As stated I am assuming sanitized, valid type and structures
    if (is_string($tournament[0][0])){//it is a single match
      $leftChampion  = $tournament[0];
      $rightChampion = $tournament[1];
    }else{//It is a nested Tourney
      //Call a match with the recursive results of both tournament siblings
      $leftChampion  = $this->_rpsTournament($tournament[0]);
      $rightChampion = $this->_rpsTournament($tournament[1]);
    }

    $response = $this->_rpsMatch (
      array (
        $leftChampion,
        $rightChampion
      )
    );

    // The first (or last) match, should return both winner and second best
    if($returnSecondPlace){
      if ($response[0] == $leftChampion[0]){ // Left Champion won the final
        $response = array($response , $rightChampion);
      }else{ // Right Champion won the final
        $response = array($response , $leftChampion);
      }
    }
    return $response;
  }


  private function _storeScore($players){
    //Look for player one in DB
    $playerScore = Score::model()->findByPk($players[0][0]);

    if(is_null($playerScore)){//If does not exists, create with 3 points
      $playerScore = new Score();
      $playerScore->name = $players[0][0];
      $playerScore->points = 3;
    }else{//found, then add 3 points
      $playerScore->points += 3;
    }
    $playerOneStatus = $playerScore->save();

    //Look for player one in DB
    $playerScore = Score::model()->findByPk($players[1][0]);

    if(is_null($playerScore)){//If does not exists, create with 1 point
      $playerScore = new Score();
      $playerScore->name = $players[1][0];
      $playerScore->points = 1;
    }else{//found, then add 1 point
      $playerScore->points += 1;
    }
    $playerTwoStatus = $playerScore->save();

    return array($playerOneStatus, $playerTwoStatus);
  }

/*
 * REST API
 *
*/


  //Stores the first and second place of a tournament, each user is stored with their respective scores.
  //The user names will be unique, but the same user can win 1 or more championships.
  //Returns the operation status if successfully.
  //http method: POST Response format: JSON
  //POST data: first=Dave&second=Armando
  //POST return: {status: 'success'}

  /* TODO: I know I should not be using varchar as primary key for score table
   * I do it just because this app will never run with real users.
   * optimizing here would be a waste of time
  */
  public function actionResult(){
    $json = file_get_contents('php://input'); //$GLOBALS['HTTP_RAW_POST_DATA'] is not preferred: http://www.php.net/manual/en/ini.core.php#ini.always-populate-raw-post-data
    $splitPlayers = explode('&',$json);
    $splitPlayers[0] = array_reverse(explode('=',$splitPlayers[0]));
    $splitPlayers[1] = array_reverse(explode('=',$splitPlayers[1]));

    //$this->requestBody();
    $status = $this->_storeScore($splitPlayers);
    if($status[0]!=false && $status[1] != false){
      $this->_sendResponse(200, CJSON::encode(array("status"=>"succcess")));
    } else {
      $this->_sendResponse(400, CJSON::encode(array("status"=>"failed")));
    }
  }

  //Retrieves Top players of all championships.
  //Returns the list of player names based on the count provided.
  //Limit defaults to 10
  // This should not be a Get operation, but a LIST instead
  public function actionTop(){
    $limit = isset ($_GET['count'])? $_GET['count']:10;

    // this SHOULD be in the model, but in this Yii setup, models are autogenerated
    //I do not want anybody to overwrite it thus making it fail
    // In a real live environment I would configure Yii to inherit from auto generate model and extend with custom functions

    $connection=Yii::app()->db;
    $sql = "SELECT * FROM score ORDER BY points DESC limit 0,".$limit;
    $command=$connection->createCommand($sql);
    $rows=$command->queryAll();
    foreach($rows as $row){
      $playerNames[] = $row['name'];
    }
    $players = array("players"=>$playerNames);
    //var_dump($playerNames);
    //exit();
    $this->_sendResponse(200, CJSON::encode($players));
  }

  // Receives the championship data and computes it to identify the winner.
  // The first and second place are stored into a database with their respective scores
  // Returns the winner of the championship
  public function actionNew(){
    $this->requestBody();
    $winner = $this->_rpsTournament($this->requestBody,true);
    $this->_storeScore($winner);
    $this->_sendResponse(200, CJSON::encode($winner[0]));
  }

  //Clear the score table
  public function actionClear(){
    $connection=Yii::app()->db;
    $sql = "TRUNCATE `score`";
    $command=$connection->createCommand($sql);
    $rows=$command->execute();
    $this->_sendResponse(200, "DB cleared");
  }

  public function actionTest(){
    //$this->requestBody();
    //$winner = $this->_rpsTournament($this->requestBody,true);
    //$this->_storeScore($winner);

    $happy= '[
    {
      "age": 0, 
        "id": "motorola-xoom-with-wi-fi", 
        "imageUrl": "img/phones/motorola-xoom-with-wi-fi.0.jpg", 
        "name": "Motorola XOOM\u2122 with Wi-Fi", 
        "snippet": "The Next, Next Generation\r\n\r\nExperience the future with Motorola XOOM with Wi-Fi, the worlds first tablet powered by Android 3.0 (Honeycomb)."
    }, 
    {
      "age": 1, 
        "id": "motorola-xoom", 
        "imageUrl": "img/phones/motorola-xoom.0.jpg", 
        "name": "MOTOROLA XOOM\u2122", 
        "snippet": "The Next, Next Generation\n\nExperience the future with MOTOROLA XOOM, the worlds first tablet powered by Android 3.0 (Honeycomb)."
    }, 
    {
      "age": 2, 
        "carrier": "AT&amp;T", 
        "id": "motorola-atrix-4g", 
        "imageUrl": "img/phones/motorola-atrix-4g.0.jpg", 
        "name": "MOTOROLA ATRIX\u2122 4G", 
        "snippet": "MOTOROLA ATRIX 4G the worlds most powerful smartphone."
    }, 
    {
      "age": 3, 
        "id": "dell-streak-7", 
        "imageUrl": "img/phones/dell-streak-7.0.jpg", 
        "name": "Dell Streak 7", 
        "snippet": "Introducing Dell\u2122 Streak 7. Share photos, videos and movies together. It\u2019s small enough to carry around, big enough to gather around."
    }, 
    {
      "age": 4, 
        "carrier": "Cellular South", 
        "id": "samsung-gem", 
        "imageUrl": "img/phones/samsung-gem.0.jpg", 
        "name": "Samsung Gem\u2122", 
        "snippet": "The Samsung Gem\u2122 brings you everything that you would expect and more from a touch display smart phone \u2013 more apps, more features and a more affordable price."
    }
]
';

    $this->_sendResponse(200, $happy);
  }
  
  public function actionList(){
    $this->_sendResponse(501, 'no List implemented' );
  }

  public function actionCreate(){
    $this->_sendResponse(501, 'no Create implemented' );
  }

  public function actionUpdate(){
    $this->_sendResponse(501, 'no Update implemented' );
  }

  public function actionDelete(){
    $this->_sendResponse(501, 'no Delete implemented' );
  }

}